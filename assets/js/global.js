/* Author:http://www.rainatspace.com

*/

function initializeScript(){
	var WinWidth = jQuery(window).width();

	//jQuery('#preloader').fadeOut(2000);
		jQuery("[icon-data]").each(function(){
		var getAttr =  jQuery(this).attr('icon-data');
		jQuery(this).addClass(getAttr).removeAttr('icon-data');
	});

	// NAVIGATION RESPOSNIVE HANDLER
	jQuery(".nav ul").clone(false).appendTo(".nav-rwd-sidebar");
	jQuery(window).on('load', function(){
		jQuery('.nav-rwd-sidebar').find('ul').removeClass();
	});
	jQuery(".btn-rwd-sidebar, .btn-hide").click( function() {
		jQuery(".nav-rwd-sidebar").toggleClass("sidebar-active");
		jQuery(".wrapper-inner").toggleClass("wrapper-active");
	});

	// Navigation  Bounce Menu Effect================================================================*/
	jQuery('.nav li ul').removeClass('hidden');
		jQuery('.nav li').hover(function() {
			jQuery('ul', this).filter(':not(:animated)').slideDown(600, 'easeOutBounce');
	     }, function() {
		jQuery('ul', this).slideUp(600, 'easeInExpo');
	});


	jQuery("#tabs li").click(function() {
		jQuery("#tabs li").removeClass('active');
		jQuery(this).addClass("active");
		jQuery(".tab_content").hide();
		var selected_tab = jQuery(this).find("a").attr("href");
		jQuery(selected_tab).fadeIn();
		return false;
	});


	//FIXED NAV
	jQuery(window).scroll(function() {
		if(WinWidth > 779){
			if (jQuery(this).scrollTop() > 1){  
		        jQuery('#header').addClass("sticky");
		    }
		    else{
		        jQuery('#header').removeClass("sticky");
		    }
	    }
	});
}

/* =Document Ready Trigger
-------------------------------------------------------------- */
jQuery(document).ready(function(){
    initializeScript();

    jQuery('.dropdown-toggle').dropdown();

	//Fancybox
	jQuery('.fancybox').fancybox({
		maxWidth	: 600,
		//maxHeight	: 600,
		fitToView	: true,
		//width		: '70%',
		//height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});

	jQuery('#full-width-slider').royalSlider({
	    arrowsNav: true,
	    controlNavigation: 'bullets',
	    loop: true,
	    keyboardNavEnabled: true,
	    controlsInside: false,
	    imageScaleMode: 'fill',
	    arrowsNavAutoHide: false,
	    autoScaleSlider: true, 
	    autoScaleSliderWidth: 1400,     
	    autoScaleSliderHeight: 700,
	    controlNavigation: 'bullets',
	    thumbsFitInViewport: false,
	    navigateByClick: true,
	    startSlideId: 0,
	    autoPlay: false,
	    transitionType:'fade',
	    globalCaption: false,
	    deeplinking: {
	      enabled: true,
	      change: false
	    },
	    /* size of all images http://help.dimsemenov.com/kb/royalslider-jquery-plugin-faq/adding-width-and-height-properties-to-images */
	    imgWidth: 1600,
	    imgHeight: 750
	  });

});
/* END ------------------------------------------------------- */